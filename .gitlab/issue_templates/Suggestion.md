## Summary

<!-- Please tell us about your idea -->

## Is this related to a particular module or assignment? Which one?

<!-- Please delete if not applicable -->

## Is this something you would like to help build or add to this repo?

<!-- Yes/No -->

<!-- Please do not remove the line below! -->

/label ~action::triage ~faq::suggestion
